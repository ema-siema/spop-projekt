{-
Autorzy: Radosław Kołecki & Asia Wetesko
15 maja 2017
Projekt z języka Haskell wykonany w ramach przedmiotu SPOP - semestr 17L
-}

module Spop where
import Gui
import Model
import System.Directory
import System.IO.Error
import Control.Exception

main = do
  putStrLn "\nŁamigłówka typu plaster miodu. Autorzy: Radosław Kołecki & Asia Wetesko.\n"
  askForFile
      
askForFile = do 
  putStrLn "Podaj nazwę pliku wejściowego z łamigłówką:"
  plasterName <- getLine
  fileOK <- doesFileExist plasterName
  if (fileOK)
    then do
      pls <- readFile plasterName
      let plasterChar = (read pls) :: Plaster 
          plaster = mapToPlasterCell plasterChar
      --print plasterChar
      d <- catch (drawBoard plaster (0, 0)) wrongFormat
      putStrLn ""
      solve plaster (0, 0) A 
      putStrLn ""
    else do
      putStrLn "Nie znaleziono podanego pliku."
      askForFile
      
wrongFormat :: SomeException -> IO ()
wrongFormat _ = do 
  putStrLn "Podany plik ma zły format!"
  askForFile

solve :: PlasterCell -> Position -> Cell -> IO (String)
solve plaster pos letter
  | pos == maxPos plaster && isLetter plaster pos = do
      putStrLn "\nZnalezione rozwiązanie: "
      drawBoard plaster (0, 0)
      return "fin"
      
  | isLetter plaster pos = do
      solve plaster (nextPos plaster pos) letter
  | otherwise = do
      let l = findLetters plaster letter pos
      if l == [] then do
        return "nawrot"
      else do
        s <- solve (newBoard plaster pos (l !! 0)) (nextPos plaster pos) A
        if s == "nawrot" then do       
                        solve plaster pos (succ letter)
                      else do 
                        return ""

--wczytana z pliku struktura to tablice typu Char, nalezy je zmapowac na tablice typu Cell      
mapToPlasterCell:: Plaster -> PlasterCell
mapToPlasterCell plaster = PlasterCell (mapToPlasterCell' plaster)

mapToPlasterCell' :: Plaster -> [[Cell]]
mapToPlasterCell' (Plaster [])         = []
mapToPlasterCell' (Plaster (row:rows)) = [(mapRow row)] ++ mapToPlasterCell' (Plaster rows)

mapRow :: [Char] -> [Cell]
mapRow []     = []
mapRow (x:xs) = [charToCell x] ++ mapRow xs

--utworz nowy plaster z nowa litera na podanej pozycji
newBoard :: PlasterCell -> Position -> Cell -> PlasterCell
newBoard plaster pos letter = PlasterCell $ newBoard' plaster pos letter 0
      
newBoard' :: PlasterCell -> Position -> Cell -> Int -> [[Cell]]    
newBoard' (PlasterCell(row:rows)) pos letter i
  | i == fst pos = (setCellInRow row pos letter) : rows
  | otherwise = row : newBoard' (PlasterCell rows) pos letter (i+1)
  
--znajdz liste pasujacych liter nie mniejszych od podanej
findLetters :: PlasterCell -> Cell -> Position -> [Cell]
findLetters plaster letter pos 
  | letter == None = []
  | otherwise      = shortenLetterList letter (getAllowedLetters plaster pos)

--ogranicz liste pasujacych liter tak, aby odrzucic litery ktore juz byly sprawdzane 
shortenLetterList :: Cell -> [Cell] -> [Cell]
shortenLetterList letter [] = []
shortenLetterList letter list@(x:xs)
  | x < letter = shortenLetterList letter xs
  | otherwise  = list
  
module Model where

data Cell   = A | B | C | D | E | F | G | None deriving (Eq, Ord, Enum, Show, Read)
type Position = (Int, Int)
data PlasterCell = PlasterCell [[Cell]] deriving (Show, Read)
data Plaster     = Plaster [[Char]] deriving (Show, Read)

--utworz nowy rzad plastra z podmienionym elementem na podanej pozycji
setCellInRow :: [Cell] -> Position -> Cell -> [Cell]
setCellInRow row pos letter = take (i) row ++ letter : drop (i+1) row
  where i = snd pos

--zwroc komorke na podanej pozycji
getCell :: PlasterCell -> Position -> Cell
getCell (PlasterCell (row:_)) (0, b)  = row !! b
getCell (PlasterCell (_:rows)) (a, b) = getCell (PlasterCell rows) (a-1, b)   

--zwroc numer rzedu, w ktorym znajduje sie komorka o podanej pozycji
getCellRow :: PlasterCell -> Position -> Int
getCellRow plaster pos = fst $ pos

--zwroc numer pozycji w rzedzie, w ktorym znajduje sie komorka o podanej pozycji
getCellCol :: PlasterCell -> Position -> Int
getCellCol plaster pos = snd $ pos

--zwroc litere wpisana w komorke o podanej pozycji
getLetter :: PlasterCell -> Position -> Cell
getLetter plaster pos = getCell plaster pos  

--zwroc liczbe odpowiadajaca w enumeracji literze wpisanej w komorke o podanej pozycji
getLetterInt :: PlasterCell -> Position -> Int
getLetterInt plaster pos = fromEnum $ getLetter plaster pos

--czy w komorce o podanej pozycji jest juz wpisana litera?
isLetter :: PlasterCell -> Position -> Bool
isLetter plaster pos = getLetter plaster pos /= None

--czy element na podanej pozycji znajduje sie w parzystym rzedzie plastra?
isEvenRow :: Position -> Bool
isEvenRow (a,b) = mod a 2 == 0

--czy element na podanej pozycji znajduje sie w pierwszym rzedzie plastra?
isFirstRow :: PlasterCell -> Position -> Bool
isFirstRow plaster pos = getCellRow plaster pos == 0 

--czy element na podanej pozycji znajduje sie w ostatnim rzedzie plastra?
isLastRow :: PlasterCell -> Position -> Bool
isLastRow plaster pos = getCellRow plaster pos == maxRow plaster

--czy element na podanej pozycji jest pierwszym elementem w swoim rzedzie plastra?
isFirstInRow :: PlasterCell -> Position -> Bool
isFirstInRow plaster pos = getCellCol plaster pos == 0

--czy element na podanej pozycji jest ostatnim elementem w swoim rzedzie plastra?
isLastInRow :: PlasterCell -> Position -> Bool
isLastInRow plaster pos = getCellCol plaster pos == maxCol plaster pos
  
--znajdz w plastrze pozycje kolejnego indeksu po podanym (jesli skonczyl sie rzad, to przejdz do nastepnego rzedu)
nextPos :: PlasterCell -> Position -> Position
nextPos pl pos@(i, j)
  | pos == maxPos pl  = (i, j)
  | j < maxCol pl pos = (i, j+1)
  | otherwise         = (i+1, 0)

--indeks ostatniego rzedu w plastrze
maxRow :: PlasterCell -> Int
maxRow (PlasterCell [row])      = 0
maxRow (PlasterCell (row:rows)) = 1 + maxRow (PlasterCell (rows))

--indeks ostatniej komorki w rzedzie, w ktorym znajduje sie komorka o podanej pozycji
maxCol :: PlasterCell -> Position -> Int
maxCol plaster pos
  | isEvenRow pos  = maxRow plaster - 1
  | otherwise      = maxRow plaster

--pozycja ostatniej komorki w plastrze
maxPos :: PlasterCell -> Position
maxPos pl = (maxRow pl, maxCol pl (maxRow pl, 0)) 
  
--zwroc liste wszystkich liter dozwolonych na danej pozycji
getAllowedLetters :: PlasterCell -> Position -> [Cell]
getAllowedLetters plaster pos = [x | x <- [A .. G], not (elem x forbidden)]
  where forbidden = getNeighbourCells plaster pos

{-zwroc liste wszystkich elementow w odleglosci 1 lub 2 od elementu na podanej pozycji
 (nalezy sprawdzic wlasnie taki obszar, aby miec pewnosc, ze nowa litera bedzie spelniac
  zasady zagadki)-}
getNeighbourCells :: PlasterCell -> Position -> [Cell]
getNeighbourCells plaster pos = map (getCell plaster) positions
  where positions  = [x | x <- neighbours,  fst x >= 0 && 
                                            fst x <= maxRow plaster && 
                                            snd x >=0 && 
                                            snd x <= maxCol plaster x]
        neighbours = [
                     left1, left2, right1, right2, upleft, upright,
                     neightLeft upleft, neightRight upright,
                     neightUpLeft upleft, up, neightUpRight upright,
                     downleft, downright, neightLeft downleft, neightRight downright,
                     down, neightLeft down, neightRight down
                     ]
        left1   = neightLeft pos
        left2   = neightLeft left1
        right1  = neightRight pos
        right2  = neightRight right1
        upleft  = neightUpLeft pos
        upright = neightUpRight pos
        up      = neightUpRight upleft
        downleft  = neightDownLeft pos
        downright = neightDownRight pos
        down      = neightDownRight downleft
                        
--zwroc element na lewo od elementu o podanym indeksie
neightLeft:: Position -> Position  
neightLeft (a,b) = (a, b-1)
           
--zwroc element powyzej i po lewej od elementu o podanym indeksie
neightUpLeft:: Position -> Position           
neightUpLeft p@(a,b) = if isEvenRow p then (a-1, b) else (a-1, b-1)

--zwroc element powyzej i po prawej od elementu o podanym indeksie    
neightUpRight:: Position -> Position                      
neightUpRight p@(a,b) = if isEvenRow p then (a-1, b+1) else (a-1, b)
 
--zwroc element na prawo od elementu o podanym indeksie
neightRight:: Position -> Position                    
neightRight (a,b)= (a, b+1) 

--zwroc element ponizej i po prawej od elementu o podanym indeksie     
neightDownRight:: Position -> Position                       
neightDownRight p@(a,b) = if isEvenRow p then (a+1, b+1) else (a+1, b)

--zwroc element ponizej i po lewej od elementu o podanym indeksie       
neightDownLeft:: Position -> Position              
neightDownLeft p@(a,b) = if isEvenRow p then (a+1, b) else (a+1, b-1)

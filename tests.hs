module Tests where
import Test.HUnit 
import Spop
import Model
import TestData

-- run the tests with "runTestTT tests" in ghci (of course :l tests first)

foo a = (1,2)

test1 = TestCase (assertEqual "findLetters(0,0)" ([B,C,E]) (findLetters plasterTest1 A (0,0)))
test2 = TestCase (assertEqual "findLetters(0,1)" ([C,D]) (findLetters plasterTest1 A (0,1)))
test3 = TestCase (assertEqual "findLetters(0,2)" ([C]) (findLetters plasterTest1 A (0,2)))
test4 = TestCase (assertEqual "findLetters(0,3)" ([B,C,F]) (findLetters plasterTest1 A (0,3)))
test5 = TestCase (assertEqual "findLetters(1,0)" ([C,E]) (findLetters plasterTest1 A (1,0)))
test6 = TestCase (assertEqual "findLetters(2,0)" ([C]) (findLetters plasterTest1 A (2,0)))
test7 = TestCase (assertEqual "findLetters(3,0)" ([A,C]) (findLetters plasterTest1 A (3,0)))
test8 = TestCase (assertEqual "findLetters(4,0)" ([C,E,G]) (findLetters plasterTest1 A (4,0)))
test9 = TestCase (assertEqual "findLetters(4,1)" ([G]) (findLetters plasterTest1 A (4,1)))
test10 = TestCase (assertEqual "findLetters(4,2)" ([A]) (findLetters plasterTest1 A (4,2)))
test11 = TestCase (assertEqual "findLetters(4,3)" ([A,B]) (findLetters plasterTest1 A (4,3)))
test12 = TestCase (assertEqual "findLetters(2,2)" ([E]) (findLetters plasterTest1 A (2,2)))
test13 = TestCase (assertEqual "findLetters(1,4)" ([B,D]) (findLetters plasterTest1 A (1,4)))
test14 = TestCase (assertEqual "findLetters(2,3)" ([B,G]) (findLetters plasterTest1 A (2,3)))

testNeigh = [None, A, None, None, G, E, F, C]

test15 = TestCase(assertEqual "getNeighbourCells(1,4)" ((testNeigh)) (getNeighbourCells plasterTest1 (1,4)))
                     
tests = TestList [TestLabel "test1" test1,
                  TestLabel "test2" test2,
                  TestLabel "test3" test3,
                  TestLabel "test4" test4, 
                  TestLabel "test5" test5,
                  TestLabel "test6" test6,
                  TestLabel "test7" test7,
                  TestLabel "test8" test8,
                  TestLabel "test9" test9,
                  TestLabel "test10" test10,
                  TestLabel "test11" test11,
                  TestLabel "test12" test12,
                  TestLabel "test13" test13,
                  TestLabel "test13" test14,
                  TestLabel "neigh-1" test15

                  ]
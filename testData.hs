module TestData where
import Model

plasterTest1 = PlasterCell [[B,    D,    None, None],
                       [None, G,    A,    None, D],
                       [None, F,    E,    G],
                       [A,    B,    D,    C,    F],
                       [E,    None, None, None]]
                
plasterTest2 = PlasterCell [[ F, D, E, None, G, B   ],
                        [ G, None, C, None, None, None, None],
                        [ E, A, None, None, None, None],
                        [ C, None, D, E, None, None, None],
                        [ G, B, None, None, None, E   ],
                        [ D, E, None, None, B, C, F   ],
                        [ None, F, D, None, None, None]]

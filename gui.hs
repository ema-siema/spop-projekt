module Gui where
import Model

drawBoard :: PlasterCell -> Position -> IO ()
drawBoard plaster pos = do
  putStrLn ""  
  drawBoard' plaster (0,0)
  putStrLn ""  

drawBoard' :: PlasterCell -> Position -> IO (String)
drawBoard' plaster pos 
  | pos == maxPos plaster = do 
        putStr $ (stringLetter plaster pos) ++ " "
        return "ok"
  | isEvenRow pos && isFirstInRow plaster pos = do 
        putStr $ " " ++ (stringLetter plaster pos) ++ " "
        drawBoard' plaster (nextPos plaster pos)     
  | isLastInRow plaster pos = do 
        putStrLn $ (stringLetter plaster pos) ++ " "
        drawBoard' plaster (nextPos plaster pos)  
  | otherwise = do 
        putStr $ (stringLetter plaster pos) ++ " "
        drawBoard' plaster (nextPos plaster pos)

stringLetter :: PlasterCell -> Position -> String
stringLetter plaster pos = do
  enumToStringol $ getLetterInt plaster pos  

enumToStringol :: Int -> String
enumToStringol int = case int of
  0 -> "A"
  1 -> "B"
  2 -> "C"
  3 -> "D"
  4 -> "E"
  5 -> "F"
  6 -> "G"
  7 -> "."
  _ -> "X"
  
charToCell :: Char -> Cell
charToCell char = case char of
  'A' -> A
  'B' -> B
  'C' -> C
  'D' -> D
  'E' -> E
  'F' -> F
  'G' -> G
  '.' -> None
  _   -> None